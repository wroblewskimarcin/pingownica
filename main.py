# -*- coding: utf-8 -*-
"""Simmple function to chcek if www.wp.pl is available throug ping"""
import time
from pythonping import ping


def pingcheck(host):
    """General function to make the ping - 10times, 400 bytes Returns avg time in ms"""
    try:
        response_list = ping(host, size=400, count=10, timeout=1)
        final_rep = str(response_list.rtt_avg_ms)
    except:
        final_rep = 'NetworkBug'    
    return final_rep

def pingloop(host, repeater):
    """Run ping in loop with 10 sec pause"""
    for _i in range(repeater):
        print(host+
              '; '+str(time.ctime())+
              '; '+pingcheck(host))
        time.sleep(10)

if __name__ == "__main__":
    pingloop('www.wp.pl', 15)
